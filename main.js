// this grabs elemnt by id from html, it is an object and has a lot of diff data points
// onclick is what happens whenever you lcikc on it/element in general; doesnt have to be a button; could add to word or rim of square or w/e
// i am making new function to attach to onclick, which is why all the code needs to go on the inside
document.getElementById("countButton").onclick = function() {
    // all of the code given goes inside here because it happens on the click button and goes
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    // This changes all the letter to lower case.

    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    // This gets rid of all the characters except letters, spaces, and apostrophes.
    // ^ is begginning of string; g is global modifier; + checks if string is integer; \s is whitespace characer/space; \S is not whitespace character; a-z is find any char from a-z/characters from inside the brackets; 


    // LETTERS
    // this needs to be above the for typedtext.length loop because the if else has currentletter and needs to be defined
    letterCounts = {};

    for(let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];
        // do something for each letter.
        // if encountering a letter for the first time, initialize the corresponding count to 1. else, add one to the count
        if(letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }
    }

    // trying to figure out better sort=ing method
    // let output = Object.entries(letterCounts)
    //     .sort((a, b) => b[1]-a[1]) //2)
    //     .map(v => v[0]); //3) 
    // console.log(output)
    
    // output how much each letter occurs 
    for(let letter in letterCounts) {
        let span = document.createElement("span");
        // making textnode with a quote plus letter (happens in for loop and goes over lettercounts and finds them all); looks a specifc way to be readable to user
        let textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        // puts textnode on span
        span.appendChild(textContent);
        // ships it over 
        document.getElementById("lettersDiv").appendChild(span);
    }


    // WORDS
    // break input string into words by splitting on spaces
    // typedtext is lije num of characyers
    const words = typedText.split(" ");
    const wordCounts = {};

    for(let i = 0; i < words.length; i++) {
        currentWord = words[i];
        if(wordCounts[currentWord] === undefined) {
            wordCounts[currentWord] = 1;
        } else {
            wordCounts[currentWord]++;
        }
    }
    for(let word in wordCounts) {
        let span = document.createElement("span");
        let textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", ");
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
    }
}